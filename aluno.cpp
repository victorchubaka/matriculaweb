#include "aluno.hpp"
#include <iostream>

Aluno::Aluno(){
    set_nome("");
    set_telefone("0000");
    set_cpf(123);
    set_matricula(0);
    set_curso("Eng.");
    set_ira(5.0f);
    cout << "Contrutor da classe Aluno" << endl;
}
Aluno::~Aluno(){
    cout << "Destrutor da classe Aluno" << endl;

}
long Aluno::get_matricula(){
    return matricula;
}
void Aluno::set_matricula(long matricula){
    this->matricula = matricula;
}
string Aluno::get_curso(){
    return curso;
}
void Aluno::set_curso(string curso){
    this->curso = curso;
}
float Aluno::get_ira(){
    return ira;
}
void Aluno::set_ira(float ira){
    this->ira = ira;
}
void Aluno::imprime_dados(){
    cout << "Nome: " << get_nome() << endl;
    cout << "Telefone: " << get_telefone() << endl;
    cout << "CPF: " << get_cpf() << endl;
    cout << "Matricula: " << get_matricula() << endl;
    cout << "Curso: " << get_curso() << endl;
    cout << "IRA: " << get_ira() << endl;
}












