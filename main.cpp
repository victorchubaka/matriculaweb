#include <iostream> 
#include "pessoa.hpp"
#include "aluno.hpp"

using namespace std;

int main(int argc, char ** argv){

    Pessoa pessoa1;
    Pessoa * pessoa2 = new Pessoa();

    pessoa1.set_nome("João");
    pessoa1.set_cpf(23412354312);
    pessoa1.set_telefone("555-222");

    cout << "Nome: " << pessoa1.get_nome() << endl; 
    cout << "CPF: " << pessoa1.get_cpf() << endl;
    cout << "Telefone: " << pessoa1.get_telefone() << endl;

    delete pessoa2;

    cout << endl << "Teste da classe Aluno" << endl << endl;

    Aluno aluno1;

    aluno1.set_nome("Carlos");
    aluno1.set_telefone("333-221");
    aluno1.set_cpf(34545698733);
    aluno1.set_matricula(18039989);
    aluno1.set_curso("Eng. de Software");
    aluno1.set_ira(4.2);

    aluno1.imprime_dados();


    return 0;
}






