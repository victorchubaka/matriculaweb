#ifndef ALUNO_HPP
#define ALUNO_HPP

#include <string>
#include "pessoa.hpp"

using namespace std;

class Aluno : public Pessoa {
    private:
        long matricula;
        string curso;
        float ira;
    public:
        Aluno();
        ~Aluno();
        long get_matricula();
        void set_matricula(long matricula);
        string get_curso();
        void set_curso(string curso);
        float get_ira();
        void set_ira(float ira);

        void imprime_dados();
};

#endif







